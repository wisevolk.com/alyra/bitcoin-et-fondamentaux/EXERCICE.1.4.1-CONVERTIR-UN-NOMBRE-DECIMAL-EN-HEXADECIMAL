const regexDecimal = /^[0-9]+/;
const regexHexa = /([a-f]+\d*)|(\d*[a-f]+)/i;

//Conversion de nombres décimaux et hexa en little endian

const decimalToHex = (decimal) => {
    try {
        if(!regexDecimal.test(decimal))
            throw new Error("La valeur soumise n'est pas un entier");
        const hexNumber = [];
        while ( decimal !== 0 ) {
            const reste = Math.trunc(decimal / 16);
            let hex = decimal - (reste * 16);
            if ( hex > 9 )
                hex = String.fromCharCode(hex + 55);

            //console.log(hex);
            hexNumber.push(hex);
            decimal = Math.trunc(decimal / 16);
            // console.log("hex : "+hexNumber.reverse().join(""));
            return hexNumber.reverse().join("");
        }
    } catch (e) {
        return e.message;
    }
}

console.log(decimalToHex(3216567));
console.log(decimalToHex("ff3462"));
console.log(decimalToHex("5876"));
